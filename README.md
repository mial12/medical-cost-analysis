Data analysis and visualization project implemented in R. 
Predicts individual health insurance charges using linear regression models. Based on data from Kaggle (https://www.kaggle.com/datasets/mirichoi0218/insurance).
